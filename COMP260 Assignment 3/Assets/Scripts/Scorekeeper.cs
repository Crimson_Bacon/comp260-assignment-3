﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scorekeeper : MonoBehaviour {
	
	static private Scorekeeper instance;
	static public Scorekeeper Instance {
		get {return instance;}
	}
	
	public Text alliesCollectedText;
	public Text totalAlliesText;
	
	private int alliesCollected;
	public static int totalAllies;

	// Use this for initialization
	void Start () {
		if(instance == null){
			instance = this;
		}
		else {
			Debug.LogError("More than one Scorekeeper exists in the scene.");
		}
		totalAlliesText.text = totalAllies.ToString();
		alliesCollectedText.text = "0";
		alliesCollected = 0;
	}
	
	// Update is called once per frame
	public void OnCollectAlly () {
		alliesCollected++;
		totalAllies ++;
		alliesCollectedText.text = alliesCollected.ToString();
	}
	
	public int numOfAlliesCollected(){
			return alliesCollected;
	}
	
	public int numOfAlliesNeeded(int a){
		if(a == 0){
			return 1;
		}
		else if(a == 1){
			return 4;
		}
		else if(a == 2){
			return 3;
		}
		else {
			return 0;
		}
	}
}
