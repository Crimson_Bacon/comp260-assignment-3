﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioPlayer : MonoBehaviour {
	static private AudioPlayer instance;
	static public AudioPlayer Instance {
		get {return instance;}
	}
	
	public AudioClip collectAllyClip;
	public AudioClip deathClip;
	private AudioSource audio;

	// Use this for initialization
	void Start () {
		if(instance == null){
			instance = this;
		}
		else {
			Debug.LogError("More than one Scorekeeper exists in the scene.");
		}
		
		audio = GetComponent<AudioSource>();
	}
	
	public void playAllyClip(){
		audio.PlayOneShot(collectAllyClip);
	}
	
	public void playDeathClip() {
		audio.PlayOneShot(deathClip);
	}
}
