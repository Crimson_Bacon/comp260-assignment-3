﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConeDetectionHorizontal : MonoBehaviour
{
    public GameObject player;
    public GameObject startPos;
    public GameObject detectedPanel;
    public LayerMask playerLayer;
	public GameObject enemy;
	public int dir;
	
	private Rigidbody2D rb;
	private Rigidbody2D enemyRB;
	private int rotZ;
	private Vector3 v;
	private float waitTime = 1.0f;
	public float timer = 0.0f;

    // Use this for initialization
    void Start()
    {
		rotZ = 0;
		v = transform.rotation.eulerAngles;		
		detectedPanel.SetActive(false);
		rb = GetComponent<Rigidbody2D>();
		enemyRB = enemy.GetComponent<Rigidbody2D>();		
    }

    // Update is called once per frame
    void Update()
    {
		transform.rotation = Quaternion.Euler(v.x, v.y, rotZ);
		rb.position = enemyRB.position;
		dir = enemyMoveHorizontal.moveDir;
		if(dir == -1){
			rotZ = 90;
		}
		else {
			rotZ = -90;
		}
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        //when player enters detection radius, the target color is red
        if (playerLayer.Contains(col.gameObject))
        {
            detectedPanel.SetActive(true);
            AudioPlayer.Instance.playDeathClip();
			StartCoroutine(restart(waitTime));
        }
    }
	
	private IEnumerator restart(float p){
		//i want it to have a slight delay before restarting.		
		//first, stop time so the player cannot move.
		Time.timeScale = 0;
		timer = 0;
		
		//then set a timer using realtimeSinceStartup because it is unaffected by timeScale.
		timer = Time.realtimeSinceStartup + p;
		
		//while the realtime is still less than the timer, do nothing.
		while(Time.realtimeSinceStartup < timer){
			yield return 0;
		}
		
		//after while loop finishes, set player pos to its starting pos and resume timeScale.
		player.transform.position = startPos.transform.position;
		Time.timeScale = 1;
		Start();
	}
}
