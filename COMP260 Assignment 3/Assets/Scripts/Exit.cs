﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Exit : MonoBehaviour {
	
	public GameObject interactPanel;
	public GameObject cantExitPanel;
	public LayerMask playerLayer;
	
	private bool playerIsThere;
	private int alliesCollected;
	private int alliesNeeded;
	public int totalAllies;
	
	public int index;

	// Use this for initialization
	void Start () {
		interactPanel.SetActive(false);
		cantExitPanel.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
		totalAllies = Scorekeeper.totalAllies;
		alliesNeeded = Scorekeeper.Instance.numOfAlliesNeeded(index);
		alliesCollected = Scorekeeper.Instance.numOfAlliesCollected();
		if(playerIsThere){
			interactPanel.SetActive(true);
			if(Input.GetButtonDown("Interact")){
				interactPanel.SetActive(false);
				
				if(alliesCollected < alliesNeeded){
					cantExitPanel.SetActive(true);
				}
				else {					
					index++;
					SceneManager.LoadScene(index);
				}
			}
		}
		else {
			interactPanel.SetActive(false);
			cantExitPanel.SetActive(false);
		}
	}
	
	void OnTriggerEnter2D (Collider2D col){
		if(playerLayer.Contains(col.gameObject)){
			playerIsThere = true;
		}
		
	}
	
	void OnTriggerExit2D (Collider2D col){
		if(playerLayer.Contains(col.gameObject)){
			playerIsThere = false;
		}
	}
}
