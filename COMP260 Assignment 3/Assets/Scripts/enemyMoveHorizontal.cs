﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyMoveHorizontal : MonoBehaviour {
	
	public GameObject rightOrUpStop;
	public GameObject leftOrDownStop;
	
	private Rigidbody2D rb;
	private Vector2 thisPos;
	private int speed = 3;
	public static int moveDir = -1;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D>();
		rb.position = new Vector2(rightOrUpStop.transform.position.x, rightOrUpStop.transform.position.y);
	}
	
	// Update is called once per frame
	void FixedUpdate () {
			rb.MovePosition(transform.position + (transform.right*moveDir) * Time.deltaTime * speed);
			
		if(rb.position.x >= rightOrUpStop.transform.position.x){
			rb.position = new Vector2(rightOrUpStop.transform.position.x - 0.1f, rightOrUpStop.transform.position.y);
			moveDir *= -1;
		}
		else if(rb.position.x <= leftOrDownStop.transform.position.x){
			rb.position = new Vector2(leftOrDownStop.transform.position.x + 0.1f, leftOrDownStop.transform.position.y);
			moveDir *= -1;
		}
	}
}
