﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpin : MonoBehaviour
{
    public float spinSpeed = 1;
	private Rigidbody2D rb;

	// Use this for initialization
	void Start ()
    {
		rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.Rotate(0f, 0f, spinSpeed);
    }
}
