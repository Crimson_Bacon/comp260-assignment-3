﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AllyCollector : MonoBehaviour {
	
	public LayerMask playerLayer;
	

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	void OnTriggerEnter2D (Collider2D col){
		if(playerLayer.Contains(col.gameObject)){
			AudioPlayer.Instance.playAllyClip();
			Scorekeeper.Instance.OnCollectAlly();
			DestroyGameObject();
		}
	}
	
	void DestroyGameObject() {
		Destroy(gameObject);
	}
}
