﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectPointState : StateMachineBehaviour
{
    override public void OnStateEnter (Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        EnemyMove enemyMove = animator.gameObject.GetComponent <EnemyMove>();
        enemyMove.SetNextPoint();
    }
}
