﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class testEnemyMove : MonoBehaviour {
	
	public GameObject rightOrUpStop;
	public GameObject leftOrDownStop;
	public bool isMovingHorizontally;
	
	private Rigidbody2D rb;
	private Vector2 thisPos;
	private int speed = 3;
	public static int moveDir = -1;

	// Use this for initialization
	void Start () {
		Debug.Log(moveDir);
		if(rightOrUpStop.transform.position.y == leftOrDownStop.transform.position.y){
			isMovingHorizontally = true;
		}
		else {
			isMovingHorizontally = false;
		}
		rb = GetComponent<Rigidbody2D>();
		rb.position = new Vector2(rightOrUpStop.transform.position.x, rightOrUpStop.transform.position.y);
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		Debug.Log(moveDir+"1");
		if(isMovingHorizontally){
			rb.MovePosition(transform.position + (transform.right*moveDir) * Time.deltaTime * speed);
			
			if(rb.position.x >= rightOrUpStop.transform.position.x){
				rb.position = new Vector2(rightOrUpStop.transform.position.x - 0.1f, rightOrUpStop.transform.position.y);
				moveDir *= -1;
			}
			else if(rb.position.x <= leftOrDownStop.transform.position.x){
				rb.position = new Vector2(leftOrDownStop.transform.position.x + 0.1f, leftOrDownStop.transform.position.y);
				moveDir *= -1;
			}
		}
		else {
			rb.MovePosition(transform.position + (transform.up*moveDir) * Time.deltaTime * speed);
			
			if(rb.position.y <= leftOrDownStop.transform.position.y){
				rb.position = new Vector2(leftOrDownStop.transform.position.x, leftOrDownStop.transform.position.y + 0.1f);
				moveDir *= -1;
			}
			else if(rb.position.y > rightOrUpStop.transform.position.y){
				Debug.Log("yup");
				rb.position = new Vector2(rightOrUpStop.transform.position.x, rightOrUpStop.transform.position.y - 0.1f);
				moveDir *= -1;
			}
		}
	}
}
