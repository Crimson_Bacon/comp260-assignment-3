﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMove : MonoBehaviour
{
    private Transform playerPos;
    Vector3 dir;
    private float moveSpeed = 2f;
    private int curTarget;
    private Transform[] points = null;


    private Animator animator;
    bool chasing = false;
    bool idle = false;
    private float distanceFromPoint;
    public bool inCone;
	
	private Rigidbody2D rb;
	
	void Start()
    {
        playerPos = GameObject.FindGameObjectWithTag("Player").transform;

        Transform point1 = GameObject.Find("point1").transform;
        Transform point2 = GameObject.Find("point2").transform;
        points = new Transform[2]
        {
            point1,
            point2
        };
    }

    void Update()
    {
        if (chasing)
        {
            dir = playerPos.position - transform.position;
            rotateEnemy();
        }
        if (!idle)
        {
            transform.Translate(moveSpeed * dir * Time.deltaTime, Space.World);
        }
    }

    private void FixedUpdate()
    {
        distanceFromPoint = Vector3.Distance(points[curTarget].position, transform.position);
        animator.SetFloat("distanceFromPoint", distanceFromPoint);
        animator.SetBool("playerInView", inCone);
    }

    public void SetNextPoint()
    {
        int nextPoint = -1;
        do
        {
            nextPoint = Random.Range(0, points.Length - 1);
        }
        while (nextPoint == curTarget);
        curTarget = nextPoint;
        dir = points[curTarget].position - transform.position;
        rotateEnemy();
    }

    public void Chase()
    {
        dir = playerPos.position - transform.position;
        rotateEnemy();
    }

    public void StopChasing()
    {
        chasing = false;
    }

    private void rotateEnemy()
    {
        float ang = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, ang - 90));
        dir = dir.normalized;
    }

    public void StartChasing()
    {
        chasing = true;
    }

    public void ToggleIdle()
    {
        idle = !idle;
    }
}