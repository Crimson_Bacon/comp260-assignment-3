﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallMove : MonoBehaviour {
	
	public GameObject player;
	public LayerMask playerLayer;
	private Rigidbody2D rigidbody;
	private Rigidbody2D playerRB;
	
	private float speed = 10.0f;
	private bool isKicking;

	// Use this for initialization
	void Start () {
		rigidbody = GetComponent<Rigidbody2D>();
		playerRB = player.GetComponent<Rigidbody2D>();
		isKicking = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(isKicking){
			if(Input.GetButtonDown("Kick")){
				Debug.Log("hi");
				rigidbody.MovePosition(rigidbody.position + playerRB.position);
			}
		}
	}
	
	void OnCollisionEnter2D (Collision2D col){
		if(playerLayer.Contains(col.gameObject)){
			isKicking = true;
		}
	}
	
	void OnCollisionExit2D (Collision2D col){
		if(playerLayer.Contains(col.gameObject)){
			isKicking = false;
		}
	}
}
