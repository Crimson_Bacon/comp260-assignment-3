﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Detection : MonoBehaviour {
	/*
		What this class does: Detects whether or not player is in the detection radius.
		
		When player enters detection radius, the radius starts changing color from yellow to red.
		When player leaves the detection radius, the radius starts changing from red to yellow.
		When the radius is red, that means the player has been detected.
		Upon detection, a panel will pop up with the text telling you that you've been detected, and after a 
		delay, the player will return to the start position at the beginning of the level.
		
	*/
	
	public GameObject player;
	public GameObject startPos;
	public GameObject detectedPanel;
	public LayerMask playerLayer;
	
	//the timers for post-detection delay
	private float waitTime = 1.0f;
	public float timer = 0.0f;
	
	//variables to handle color change
	public Color startColor, endColor;
	Color targetColor;
	MeshRenderer myRenderer;
	//transition time to go from yellow to red & vice versa.
	private float transTime = 0.5f;

	// Use this for initialization
	void Start () {
		//don't show the "you're detected" text at the beginning.
		detectedPanel.SetActive(false);

		myRenderer = GetComponent<MeshRenderer>();
		myRenderer.material.color = startColor;
		targetColor = startColor;
	}
	
	void Update(){
		//Make the color transition 
		myRenderer.material.color = Color.Lerp(myRenderer.material.color, targetColor, Time.deltaTime/transTime);	
	}
	
	void OnTriggerEnter2D(Collider2D col){
		//when player enters detection radius, the target color is red
		if(playerLayer.Contains(col.gameObject)){
			targetColor = endColor;		
		}				
	}
	
	void OnTriggerStay2D(Collider2D col){
		//while player is staying inside the detection radius, start a timer.
		//the timer is the same as the transition time in Color.Lerp 
		if(playerLayer.Contains(col.gameObject)){
			timer += Time.deltaTime/transTime;
		
			//Once Lerp finishes executing (transition time in Lerp reaches 1), player has been detected.
			if(timer >= 1.0f){
				detectedPanel.SetActive(true);
				AudioPlayer.Instance.playDeathClip();
				
				//this is to call the IEnumerator 
				StartCoroutine(restart(waitTime));
			}
		}		
	}
	
	void OnTriggerExit2D(Collider2D col){
		//when player leaves detection radius, the target color is yellow
		if(playerLayer.Contains(col.gameObject)){
			targetColor = startColor;
		
			//reset the timer
			timer = 0;		
		}
		
	}
	
	private IEnumerator restart(float p){
		//i want it to have a slight delay before restarting.		
		//first, stop time so the player cannot move.
		Time.timeScale = 0;
		timer = 0;
		
		//then set a timer using realtimeSinceStartup because it is unaffected by timeScale.
		timer = Time.realtimeSinceStartup + p;
		
		//while the realtime is still less than the timer, do nothing.
		while(Time.realtimeSinceStartup < timer){
			yield return 0;
		}
		
		//after while loop finishes, set player pos to its starting pos and resume timeScale.
		player.transform.position = startPos.transform.position;
		Time.timeScale = 1;
		Start();
	}
}
