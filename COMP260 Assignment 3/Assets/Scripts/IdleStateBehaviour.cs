﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleStateBehaviour : StateMachineBehaviour
{
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        EnemyMove enemyMove = animator.gameObject.GetComponent<EnemyMove>();
        enemyMove.ToggleIdle();
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        EnemyMove enemyMove = animator.gameObject.GetComponent<EnemyMove>();
        enemyMove.ToggleIdle();
    }
}
