﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseStateBehaviour : StateMachineBehaviour
{
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        EnemyMove enemyMove = animator.gameObject.GetComponent<EnemyMove>();
        enemyMove.StartChasing();
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        EnemyMove enemyMove = animator.gameObject.GetComponent<EnemyMove>();
        enemyMove.StopChasing();
    }
}
